w_title(guile-gnome-0)dnl
w_section(1)dnl
w_author(Andreas Rottmann)dnl
w_man_desc(starts guile and sets it up for loading guile-gnome modules)

! SYNOPSIS

''guile-gnome-0'' [ /guile options/ ]

! DESCRIPTION

The purpose of this script is twofold. First, it serves to find
''guile'' in the load path instead of statically "linking" to
/usr/bin/guile. Second, it will set up the guile load path and the
library path such that using guile-gnome modules will /Just
Work/. This is necessary because we might choose to break the
application interfaces in the future, so we need to allow for parallel
installs.

Note that the proper way to make a script that will use this as the
interpreter is as follows:

{{{
#! /bin/sh
exec guile-gnome-0 -s $0 "$@"
!#
}}}

This will allow the shell to find ''guile-gnome-0'' in the path.

! ENVIRONMENT

GUILE_LOAD_PATH::
LD_LIBRARY_PATH::
	These are extended (or set, if unset or empty) before
	''exec''-ing ''guile''.

! SEE ALSO

''guile'' (1)
