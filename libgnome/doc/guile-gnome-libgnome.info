This is guile-gnome-libgnome.info, produced by makeinfo version 6.3 from
guile-gnome-libgnome.texi.

This manual is for '(gnome libgnome)' (version 2.16.5, updated 24
January 2015)

   Copyright 2001-2007 Kjartan Maraas, Malcolm Tredinnick, and others

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU General Public License, Version
     2 or any later version published by the Free Software Foundation.

INFO-DIR-SECTION The Algorithmic Language Scheme
START-INFO-DIR-ENTRY
* Guile-Libgnome: (guile-gnome-libgnome.info).  A GNOME catchall library.
END-INFO-DIR-ENTRY


File: guile-gnome-libgnome.info,  Node: Top,  Next: Overview,  Up: (dir)

Guile-Libgnome
**************

This manual is for '(gnome libgnome)' (version 2.16.5, updated 24
January 2015)

   Copyright 2001-2007 Kjartan Maraas, Malcolm Tredinnick, and others

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU General Public License, Version
     2 or any later version published by the Free Software Foundation.

* Menu:

* Overview::             About libgnome and its Guile bindings.

* gnome-help::           Display application and GNOME system help.
* gnome-init::           Functions used during initialisation of libgnome and other platform libraries.
* gnome-program::        Initialize and retrieve information about a GNOME application.
* gnome-sound::          Sound playing routines.
* gnome-url::            Display urls using appropriate viewers.

* Undocumented::        Undocumented functions.

* Type Index::
* Function Index::


File: guile-gnome-libgnome.info,  Node: Overview,  Next: gnome-help,  Prev: Top,  Up: Top

1 Overview
**********

'(gnome gnome)' wraps some pieces of the libgnome library for Guile.  It
is a part of Guile-GNOME.

   Libgnome historically was a staging ground for code meant to go
elsewhere.  In the modern environment, it has largely been replaced, its
functionality having been pushed into GLib, GTK+, and other lower-level
libraries.

   However, as of 2008 it still has some utility.  The help functions
are useful.  '(gnome gnome)' wraps those parts that have not been
replaced yet.

   See the documentation for '(gnome gobject)' for more information on
Guile-GNOME.


File: guile-gnome-libgnome.info,  Node: gnome-help,  Next: gnome-init,  Prev: Overview,  Up: Top

2 gnome-help
************

Display application and GNOME system help.

2.1 Overview
============

These functions provide a way to display help files that are either
installed as part of the main GNOME installation or that are specific to
the current application.

2.2 Usage
=========

 -- Function: gnome-help-display (file_name 'mchars') (link_id 'mchars')
          =>  (ret 'bool')
     Displays the help file specified by FILE-NAME at location LINK-ID
     in the preferred help browser of the user.

     FILE-NAME
          The name of the help document to display.

     LINK-ID
          Can be ''#f''.  If set, refers to an anchor or section id
          within the requested document.

     ERROR
          A '<g-error>' instance that will hold the specifics of any
          error which occurs during processing, or ''#f''

     RET
          ''#t'' on success, ''#f'' otherwise (in which case ERROR will
          contain the actual error).

 -- Function: gnome-help-display-with-doc-id (program '<gnome-program>')
          (doc_id 'mchars') (file_name 'mchars') (link_id 'mchars') => 
          (ret 'bool')
     Displays the help file specified by FILE-NAME at location LINK-ID
     within the DOC-ID domain in the preferred help browser of the user.
     Most of the time, you want to call 'gnome-help-display' instead.

     This function will display the help through creating a "ghelp" URI,
     by looking for FILE-NAME in the applications installed help
     location (found by '<gnome-file-domain-app-help>') and its app_id.
     The resulting URI is roughly in the form
     "ghelp:appid/file_name?link_id".  If a matching file cannot be
     found, ''#f'' is returned and ERROR is set.

     Please note that this only displays application help.  To display
     help files from the global GNOME domain, you will want to use
     'gnome-help-display-desktop'.

     PROGRAM
          The current application object, or ''#f'' for the default one.

     DOC-ID
          The document identifier, or ''#f'' to default to the
          application ID (app_id) of the specified PROGRAM.

     FILE-NAME
          The name of the help document to display.

     LINK-ID
          Can be ''#f''.  If set, refers to an anchor or section id
          within the requested document.

     ERROR
          A '<g-error>' instance that will hold the specifics of any
          error which occurs during processing, or ''#f''

     RET
          ''#t'' on success, ''#f'' otherwise (in which case ERROR will
          contain the actual error).

 -- Function: gnome-help-display-desktop (program '<gnome-program>')
          (doc_id 'mchars') (file_name 'mchars') (link_id 'mchars') => 
          (ret 'bool')
     Displays the GNOME system help file specified by FILE-NAME at
     location LINK-ID in the preferred help browser of the user.  This
     is done by creating a "ghelp" URI, by looking for FILE-NAME in the
     system help domain ('<gnome-file-domain-help>') and it's app_id.
     This domain is determined when the library is compiled.  If a
     matching file cannot be found, ''#f'' is returned and ERROR is set.

     Please note that this only displays system help.  To display help
     files for an application, you will want to use
     'gnome-help-display'.

     PROGRAM
          The current application object, or ''#f'' for the default one.

     DOC-ID
          The name of the help file relative to the system's help domain
          ('<gnome-file-domain-help>').

     FILE-NAME
          The name of the help document to display.

     LINK-ID
          Can be ''#f''.  If set, refers to an anchor or section id
          within the requested document.

     ERROR
          A '<g-error>' instance that will hold the specifics of any
          error which occurs during processing, or ''#f''

     RET
          ''#t'' on success, ''#f'' otherwise (in which case ERROR will
          contain the actual error).

 -- Function: gnome-help-display-uri (help_uri 'mchars') => 
          (ret 'bool')
     Displays HELP-URI in the user's preferred viewer.  You should never
     need to call this function directly in code, since it is just a
     wrapper for 'gnome-url-show' and consequently the viewer used to
     display the results depends upon the scheme of the URI (so it is
     not strictly a help-only function).

     HELP-URI
          The URI to display.

     ERROR
          A '<g-error>' instance that will hold the specifics of any
          error which occurs during processing, or ''#f''

     RET
          ''#t'' on success, ''#f'' otherwise (in which case ERROR will
          contain the actual error).


File: guile-gnome-libgnome.info,  Node: gnome-init,  Next: gnome-program,  Prev: gnome-help,  Up: Top

3 gnome-init
************

Functions used during initialisation of libgnome and other platform
libraries.

3.1 Overview
============

3.2 Usage
=========

 -- Function: gnome-user-dir-get =>  (ret 'mchars')
     Retrieves the user-specific directory for GNOME apps to use
     ($HOME/.gnome2 is the usual GNOME 2 value).

     RET
          An absolute path to the directory.

 -- Function: gnome-user-private-dir-get =>  (ret 'mchars')
     Differs from 'gnome-user-dir-get' in that the directory returned
     here will have had permissions of 0700 (rwx----) enforced when it
     was created.  Of course, the permissions may have been altered
     since creation, so care still needs to be taken.

     RET
          An absolute path to the user-specific private directory that
          GNOME apps can use.

 -- Function: gnome-user-accels-dir-get =>  (ret 'mchars')
     Retrieves the user-specific directory that stores the keyboard
     shortcut files for each GNOME app.  Note that most applications
     should be using GConf for storing this information, but it may be
     necessary to use the 'gnome-user-accels-dir-get' directory for
     legacy applications.

     RET
          The absolute path to the directory.


File: guile-gnome-libgnome.info,  Node: gnome-program,  Next: gnome-sound,  Prev: gnome-init,  Up: Top

4 gnome-program
***************

Initialize and retrieve information about a GNOME application.

4.1 Overview
============

4.2 Usage
=========

 -- Class: <gnome-program>
     Derives from '<gobject>'.

     This class defines the following slots:

     'app-id'
          ID string to use for this application

     'app-version'
          Version of this application

     'human-readable-name'
          Human readable name of this application

     'gnome-path'
          Path in which to look for installed files

     'gnome-prefix'
          Prefix where GNOME was installed

     'gnome-libdir'
          Library prefix where GNOME was installed

     'gnome-datadir'
          Data prefix where GNOME was installed

     'gnome-sysconfdir'
          Configuration prefix where GNOME was installed

     'app-prefix'
          Prefix where this application was installed

     'app-libdir'
          Library prefix where this application was installed

     'app-datadir'
          Data prefix where this application was installed

     'app-sysconfdir'
          Configuration prefix where this application was installed

     'create-directories'
          Create standard GNOME directories on startup

     'enable-sound'
          Enable sound on startup

     'espeaker'
          How to connect to esd

     'popt-table'
          The table of options for popt

     'popt-flags'
          The flags to use for popt

     'popt-context'
          The popt context pointer that GnomeProgram is using

     'goption-context'
          The goption context pointer that GnomeProgram is using

 -- Function: gnome-program-get =>  (ret '<gnome-program>')
     Retrieves an object that stored information about the application's
     state.  Other functions assume this will always return a
     '<gnome-program>' object which (if not ''#f'') has already been
     initialized.

     RET
          The application's '<gnome-program>' instance, or ''#f'' if it
          does not exist.

 -- Function: gnome-program-get-app-id (self '<gnome-program>') => 
          (ret 'mchars')
 -- Method: get-app-id
     This function returns a pointer to a static string that the
     application has provided as an identifier.  This is not meant as a
     human-readable identifier so much as a unique identifier for
     programs and libraries.

     PROGRAM
          The program object

     RET
          Application ID string.

 -- Function: gnome-program-get-app-version (self '<gnome-program>') => 
          (ret 'mchars')
 -- Method: get-app-version
     This function returns a pointer to a static string that the
     application has provided as a version number.  This is not meant as
     a human-readable identifier so much as a unique identifier for
     programs and libraries.

     PROGRAM
          The application object

     RET
          Application version string.


File: guile-gnome-libgnome.info,  Node: gnome-sound,  Next: gnome-url,  Prev: gnome-program,  Up: Top

5 gnome-sound
*************

Sound playing routines.

5.1 Overview
============

This module provides wrapper functions for playing sound samples.
Currently it just wraps the esound daemon, but the API is flexible
enough that other sound infrastructures can be included in the future.

   These functions also allow for the fact that no sound may be
supported on the current platform.  So applications can safely call
these functions to play sounds and they will just quietly return if no
action is possible.

5.2 Usage
=========

 -- Function: gnome-sound-init (hostname 'mchars')
     Initialize the esd connection.

     HOSTNAME
          Hostname where esd daemon resides.

 -- Function: gnome-sound-shutdown
     Shuts down the gnome sound support.

 -- Function: gnome-sound-play (filename 'mchars')
     Plays the audio stored in FILENAME, if possible.  Fail quietly if
     playing is not possible (due to missing sound support or for other
     reasons).

     FILENAME
          File containing the sound sample.

 -- Function: gnome-sound-sample-load (sample_name 'mchars')
          (filename 'mchars') =>  (ret 'int')
     Loads the audio from FILENAME and load it into the esd cache for
     later playing.  Programs will rarely want to call this function
     directly.  Use 'gnome-sound-play' instead for fire and forget sound
     playing.

     SAMPLE-NAME
          The name of the sample.

     FILENAME
          The filename where the audio is stored.

     RET
          The esound sample_id or '-1' if the sample was unable to be
          cached for esound.


File: guile-gnome-libgnome.info,  Node: gnome-url,  Next: Undocumented,  Prev: gnome-sound,  Up: Top

6 gnome-url
***********

Display urls using appropriate viewers.

6.1 Overview
============

A GNOME user can configure which viewers they wish to use to view
certain protocols.  Protocols can include http, ftp (where "view" might
mean "download"), ghelp, etc.  This module provides a means for
application to display a url without having to worry about which viewer
is going to ultimately handle the job.

6.2 Usage
=========

 -- Function: gnome-url-show (url 'mchars') =>  (ret 'bool')
     Once the input has been converted into a fully qualified url this
     function calls gnome_vfs_url_show.  Any error codes returned by
     gnome-vfs will be wrapped in the error parameter.  All errors comes
     from the 'GNOME_URL_ERROR'% domain.

     URL
          The url or path to display.  The path can be relative to the
          current working directory or the user's home directory.  This
          function will convert it into a fully qualified url using the
          gnome_url_get_from_input function.

     ERROR
          Used to store any errors that result from trying to display
          the URL.

     RET
          ''#t'' if everything went fine, ''#f'' otherwise (in which
          case ERROR will contain the actual error).


File: guile-gnome-libgnome.info,  Node: Undocumented,  Next: Type Index,  Prev: gnome-url,  Up: Top

7 Undocumented
**************

The following symbols, if any, have not been properly documented.

7.1 (gnome gnome)
=================

 -- Function: gnome-program-init name version . properties

7.2 (gnome gw libgnome)
=======================

 -- Variable: %gnome-program-init

 -- Variable: gnome-gconf-get-app-settings-relative

 -- Variable: gnome-gconf-get-gnome-libs-settings-relative

 -- Variable: gnome-program-get-human-readable-name


File: guile-gnome-libgnome.info,  Node: Type Index,  Next: Function Index,  Prev: Undocumented,  Up: Top

Type Index
**********

 [index ]
* Menu:

* <gnome-program>:                       gnome-program.        (line 14)


File: guile-gnome-libgnome.info,  Node: Function Index,  Prev: Type Index,  Up: Top

Function Index
**************

 [index ]
* Menu:

* get-app-id:                            gnome-program.       (line  89)
* get-app-version:                       gnome-program.       (line 103)
* gnome-help-display:                    gnome-help.          (line  18)
* gnome-help-display-desktop:            gnome-help.          (line  78)
* gnome-help-display-uri:                gnome-help.          (line 115)
* gnome-help-display-with-doc-id:        gnome-help.          (line  38)
* gnome-program-get:                     gnome-program.       (line  76)
* gnome-program-get-app-id:              gnome-program.       (line  86)
* gnome-program-get-app-version:         gnome-program.       (line 101)
* gnome-program-init:                    Undocumented.        (line  11)
* gnome-sound-init:                      gnome-sound.         (line  23)
* gnome-sound-play:                      gnome-sound.         (line  32)
* gnome-sound-sample-load:               gnome-sound.         (line  40)
* gnome-sound-shutdown:                  gnome-sound.         (line  29)
* gnome-url-show:                        gnome-url.           (line  20)
* gnome-user-accels-dir-get:             gnome-init.          (line  32)
* gnome-user-dir-get:                    gnome-init.          (line  15)
* gnome-user-private-dir-get:            gnome-init.          (line  22)



Tag Table:
Node: Top628
Node: Overview1631
Node: gnome-help2307
Node: gnome-init7074
Node: gnome-program8409
Node: gnome-sound11402
Node: gnome-url13092
Node: Undocumented14443
Node: Type Index14991
Node: Function Index15217

End Tag Table
